FROM haskell:8.10
WORKDIR /
COPY . .

RUN stack build
