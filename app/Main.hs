{-# LANGUAGE OverloadedStrings #-}

module Main where

import Lib
import Web.Scotty

routes :: ScottyM ()
routes = do
  get "/hello" $ do
    text "hello world"

main :: IO ()
main = do
  scotty 3000 routes
